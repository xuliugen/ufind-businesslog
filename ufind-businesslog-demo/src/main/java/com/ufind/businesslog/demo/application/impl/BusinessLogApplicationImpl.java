package com.ufind.businesslog.demo.application.impl;

import com.ufind.businesslog.demo.application.BusinessLogApplication;
import com.ufind.businesslog.demo.domain.DefaultBusinessLog;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * User: zjzhai
 * Date: 12/11/13
 * Time: 3:12 PM
 */
@Named
public class BusinessLogApplicationImpl implements BusinessLogApplication {

    @Inject
    private DefaultBusinessLog defaultBusinessLog;

    /**
     * 保存日志信息
     */
    public boolean save(DefaultBusinessLog log) {
        return this.defaultBusinessLog.save(log);
    }

}
