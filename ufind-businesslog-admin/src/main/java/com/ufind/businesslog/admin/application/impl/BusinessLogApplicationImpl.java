package com.ufind.businesslog.admin.application.impl;

import com.ufind.businesslog.admin.application.BusinessLogApplication;
import com.ufind.businesslog.admin.domain.DefaultBusinessLog;
import com.ufind.businesslog.admin.dto.DefaultBusinessLogDTO;
import com.ufind.businesslog.api.exceptions.BusinessLogBaseException;
import com.ufind.businesslog.admin.common.page.Page;
import org.springframework.beans.BeanUtils;

import javax.inject.Inject;
import javax.inject.Named;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 * User: zjzhai
 * Date: 12/11/13
 * Time: 3:12 PM
 */
@Named
public class BusinessLogApplicationImpl implements BusinessLogApplication {

    @Inject
    private DefaultBusinessLog defaultBusinessLog;

    /**
     * 根据ID获取日志信息
     */
    public DefaultBusinessLogDTO getDefaultBusinessLog(String id) {
        DefaultBusinessLog defaultBusinessLog = this.defaultBusinessLog.getById(id);
        DefaultBusinessLogDTO defaultBusinessLogDTO = new DefaultBusinessLogDTO();
        BeanUtils.copyProperties(defaultBusinessLog, defaultBusinessLogDTO);
        return defaultBusinessLogDTO;
    }

    /**
     * 更新
     */
    public void updateDefaultBusinessLog(DefaultBusinessLogDTO defaultBusinessLogDTO) {

        DefaultBusinessLog defaultBusinessLog = new DefaultBusinessLog();
        BeanUtils.copyProperties(defaultBusinessLogDTO, defaultBusinessLog);

        boolean flag = this.defaultBusinessLog.updateLog(defaultBusinessLog);
    }

    public void removeDefaultBusinessLog(String id) {
        boolean flag = this.defaultBusinessLog.removeBusinessLog(id);
    }

    public boolean save(DefaultBusinessLog log) {
        return defaultBusinessLog.save(log);
    }

    public void removeDefaultBusinessLogs(String[] ids) {
        for (int i = 0; i < ids.length; i++) {
            this.removeDefaultBusinessLog(ids[i]);
        }
    }

    /**
     * 分页获取日志信息
     * @param query       查询条件
     * @param currentPage 当前页
     * @param pageSize    页大小
     * @return
     */
    public Page<DefaultBusinessLogDTO> pageQueryDefaultBusinessLog(DefaultBusinessLogDTO query, int currentPage, int pageSize) {
        Page<DefaultBusinessLog> defaultBusinessLogPage = defaultBusinessLog.pageQueryDefaultBusinessLog(currentPage, pageSize, query);
        return this.toDTOPage(defaultBusinessLogPage, query);
    }

    private Page<DefaultBusinessLogDTO> toDTOPage(Page<DefaultBusinessLog> logPage, DefaultBusinessLogDTO query) {
        Page<DefaultBusinessLogDTO> logDTOPage = new Page<DefaultBusinessLogDTO>();
        if (logPage != null) {
            logDTOPage.setPageSize(logPage.getPageSize());
            logDTOPage.setStart(logPage.getStart());
            logDTOPage.setResultCount(logPage.getResultCount());

            List<DefaultBusinessLogDTO> logDTOs = toDTOList(logPage.getData());
            logDTOPage.setData(logDTOs);
        }
        return logDTOPage;
    }

    private static List<DefaultBusinessLogDTO> toDTOList(List<DefaultBusinessLog> data) {
        List<DefaultBusinessLogDTO> result = new ArrayList<DefaultBusinessLogDTO>();
        for (DefaultBusinessLog log : data) {
            DefaultBusinessLogDTO userDTO = new DefaultBusinessLogDTO();
            BeanUtils.copyProperties(log, userDTO);
            result.add(userDTO);
        }
        return result;
    }

}
