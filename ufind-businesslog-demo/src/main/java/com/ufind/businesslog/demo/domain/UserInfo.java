package com.ufind.businesslog.demo.domain;

import com.ufind.businesslog.demo.dto.UserInfoDTO;
import com.ufind.businesslog.demo.infra.repository.sql.UserInfoMapper;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by xuliugen on 2016/7/26.
 */
@Named
public class UserInfo extends UserInfoDTO {

    @Inject
    private UserInfoMapper userInfoMapper;

    public boolean addUser(UserInfo userInfo) {
        return userInfoMapper.insertSelective(userInfo) == 1 ? true : false;
    }
}
