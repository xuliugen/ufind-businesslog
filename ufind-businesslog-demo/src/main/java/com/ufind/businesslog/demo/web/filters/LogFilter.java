package com.ufind.businesslog.demo.web.filters;

import com.ufind.businesslog.api.BusinessLogServletFilter;
import com.ufind.businesslog.demo.domain.UserInfo;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;

/**
 * User: zjzhai
 * Date: 11/27/13
 * Time: 11:01 AM
 */
public class LogFilter extends BusinessLogServletFilter {

    /**
     * 将需要用到的信息放入日志上下文
     * @param req
     * @param resp
     * @param chain
     */
    @Override
    public void beforeFilter(ServletRequest req, ServletResponse resp, FilterChain chain) {
        addIpContext(getIp(req)); //添加IP
        HttpServletRequest request = (HttpServletRequest) req;
        UserInfo userInfo = (UserInfo) request.getSession().getAttribute("USER");
        String userAccount = userInfo == null ? "未知" : userInfo.getUserAccount();
        addUserContext(userAccount);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void destroy() {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
