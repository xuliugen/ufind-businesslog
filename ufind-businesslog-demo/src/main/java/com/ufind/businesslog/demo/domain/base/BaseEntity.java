package com.ufind.businesslog.demo.domain.base;

import org.apache.commons.collections.CollectionUtils;

import java.util.*;

/**
 * 符号常量类
 * Created by xuliugen on 16/1/10.
 */
public class BaseEntity extends CommonEntity {

    /**
     * 实体Id
     */
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 根据集合获取实体ID的集合并返回
     * @param baseEntityCollection 实体的集合
     * @return
     */
    public Set<Long> getIdSetBySelfCollection(Collection<? extends BaseEntity> baseEntityCollection) {
        Set<Long> idSet = new HashSet<Long>();
        if (CollectionUtils.isNotEmpty(baseEntityCollection)) {
            for (BaseEntity baseEntity : baseEntityCollection) {
                idSet.add(baseEntity.getId());
            }
        }
        return idSet;
    }

    /**
     * 根据集合获取集合内的数据id的Map，Map的key为id，Value为实体本身
     * @param baseEntityCollection 实体的集合
     * @return
     */
    public Map<Long, ? extends BaseEntity> getIdAndSelfMapBySelfCollection(Collection<? extends BaseEntity> baseEntityCollection) {
        Map<Long, BaseEntity> idAndSelfMap = new HashMap<Long, BaseEntity>();
        if (CollectionUtils.isNotEmpty(baseEntityCollection)) {
            for (BaseEntity baseEntity : baseEntityCollection) {
                idAndSelfMap.put(baseEntity.getId(), baseEntity);
            }
        }
        return idAndSelfMap;
    }
}