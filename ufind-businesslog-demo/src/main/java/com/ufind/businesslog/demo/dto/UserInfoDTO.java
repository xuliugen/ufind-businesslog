package com.ufind.businesslog.demo.dto;

import com.ufind.businesslog.demo.domain.base.BaseEntity;

public class UserInfoDTO extends BaseEntity {

    private String idUserInfo;

    private String userAccount;

    private String userPwd;

    private String userTypeCode;

    private String isValid;

    private String userAvatar;

    public String getIdUserInfo() {
        return this.idUserInfo;
    }

    public void setIdUserInfo(String idUserInfo) {
        this.idUserInfo = idUserInfo == null ? null : idUserInfo.trim();
    }

    public String getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(String userAccount) {
        this.userAccount = userAccount == null ? null : userAccount.trim();
    }

    public String getUserPwd() {
        return userPwd;
    }

    public void setUserPwd(String userPwd) {
        this.userPwd = userPwd == null ? null : userPwd.trim();
    }

    public String getUserTypeCode() {
        return userTypeCode;
    }

    public void setUserTypeCode(String userTypeCode) {
        this.userTypeCode = userTypeCode == null ? null : userTypeCode.trim();
    }

    public String getIsValid() {
        return isValid;
    }

    public void setIsValid(String isValid) {
        this.isValid = isValid == null ? null : isValid.trim();
    }

    public UserInfoDTO(String userAccount) {
        this.userAccount = userAccount;
    }

    public UserInfoDTO() {
    }

    public String getUserAvatar() {
        return userAvatar;
    }

    public void setUserAvatar(String userAvatar) {
        this.userAvatar = userAvatar;
    }
}