package com.ufind.businesslog.admin.dto;

import java.io.Serializable;

public class DefaultBusinessLogDTO implements Serializable {

    private static final long serialVersionUID = -6898675233659096041L;

    private String id;

    private String category;

    private String log;

    private String user;

    private String ip;

    private int version;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public DefaultBusinessLogDTO() {
    }

    public DefaultBusinessLogDTO(String id, String category, String log, String user, String ip, int version) {
        this.id = id;
        this.category = category;
        this.log = log;
        this.user = user;
        this.ip = ip;
        this.version = version;
    }
}