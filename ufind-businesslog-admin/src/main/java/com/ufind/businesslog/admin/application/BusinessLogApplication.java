package com.ufind.businesslog.admin.application;

import com.ufind.businesslog.admin.common.page.Page;
import com.ufind.businesslog.admin.domain.DefaultBusinessLog;
import com.ufind.businesslog.admin.dto.DefaultBusinessLogDTO;

import java.util.List;

/**
 * User: zjzhai
 * Date: 12/11/13
 * Time: 3:11 PM
 */
public interface BusinessLogApplication {

    public DefaultBusinessLogDTO getDefaultBusinessLog(String id);

    public void removeDefaultBusinessLog(String id);

    boolean save(DefaultBusinessLog log);

    public void removeDefaultBusinessLogs(String[] ids);

    public Page<DefaultBusinessLogDTO> pageQueryDefaultBusinessLog(DefaultBusinessLogDTO defaultBusinessLog, int currentPage, int pageSize);

}
