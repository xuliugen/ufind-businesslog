package com.ufind.businesslog.admin.infra.repository.sql;

import com.ufind.businesslog.admin.domain.DefaultBusinessLog;
import com.ufind.businesslog.admin.dto.DefaultBusinessLogDTO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by xuliugen on 2016/7/26.
 */
@Component
public interface DefaultBusinessLogMapper {

    Long insertSelective(DefaultBusinessLog log);

    int deleteByPrimaryKey(String id);

    DefaultBusinessLog selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(DefaultBusinessLog record);

    int countDefaultBusinessLog(@Param("queryCondition") DefaultBusinessLogDTO queryVo);

    List<DefaultBusinessLog> pageQueryDefaultBusinessLog(@Param("pageStart") int pageIndex, @Param("pageSize") int pageSize,
                                                         @Param("queryCondition") DefaultBusinessLogDTO queryVo);
}
