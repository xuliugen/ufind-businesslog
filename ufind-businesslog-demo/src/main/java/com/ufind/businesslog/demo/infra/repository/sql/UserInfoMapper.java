package com.ufind.businesslog.demo.infra.repository.sql;

import com.ufind.businesslog.demo.domain.base.BaseEntity;
import org.springframework.stereotype.Component;

/**
 * Created by xuliugen on 2016/7/26.
 */
@Component
public interface UserInfoMapper {

    <T extends BaseEntity> Long insertSelective(T entity);

}
