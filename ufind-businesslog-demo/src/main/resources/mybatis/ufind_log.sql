/*
Navicat MySQL Data Transfer

Source Server         : MySQL
Source Server Version : 50627
Source Host           : 127.0.0.1:3306
Source Database       : ufind_log

Target Server Type    : MYSQL
Target Server Version : 50627
File Encoding         : 65001

Date: 2016-07-29 15:17:26
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_default_bussiness_log
-- ----------------------------
DROP TABLE IF EXISTS `t_default_bussiness_log`;
CREATE TABLE `t_default_bussiness_log` (
  `id` varchar(32) NOT NULL,
  `category` varchar(255) DEFAULT NULL,
  `log` varchar(255) DEFAULT NULL,
  `user` varchar(255) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `version` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_default_bussiness_log
-- ----------------------------
INSERT INTO `t_default_bussiness_log` VALUES ('760776b7e96941ee83fd9e55fb27d176', null, 'c33bae95a2834514b9e1dc53ef31baa4-:创建一个新用户:55a9f178bc144cf484a2011401349e5a', 'c33bae95a2834514b9e1dc53ef31baa4', '127.0.0.1', '0');

-- ----------------------------
-- Table structure for t_user_info
-- ----------------------------
DROP TABLE IF EXISTS `t_user_info`;
CREATE TABLE `t_user_info` (
  `user_avatar` varchar(32) NOT NULL DEFAULT '' COMMENT '用户头像',
  `id_user_info` varchar(32) NOT NULL COMMENT '用户表，主键，uuid',
  `user_account` varchar(32) NOT NULL COMMENT '用户账号',
  `user_pwd` varchar(32) NOT NULL COMMENT '用户密码',
  `user_type_code` varchar(32) NOT NULL COMMENT '用户类型代码，外键，关联用户类型表',
  `is_valid` varchar(32) NOT NULL DEFAULT 'Y' COMMENT '是否有效，''Y''为是，''N''为否，默认''Y''',
  PRIMARY KEY (`id_user_info`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_user_info
-- ----------------------------
INSERT INTO `t_user_info` VALUES ('019fe0af5a1e44948e066eaa0f29e88d', '019fe0af5a1e44948e066eaa0f29e88d', '019fe0af5a1e44948e066eaa0f29e88d', '019fe0af5a1e44948e066eaa0f29e88d', '019fe0af5a1e44948e066eaa0f29e88d', '019fe0af5a1e44948e066eaa0f29e88d');
