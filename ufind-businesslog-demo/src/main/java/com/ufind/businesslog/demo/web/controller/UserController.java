package com.ufind.businesslog.demo.web.controller;

import com.ufind.businesslog.demo.application.UserInfoApplication;
import com.ufind.businesslog.demo.domain.UserInfo;
import com.ufind.businesslog.demo.domain.response.Response;
import com.ufind.businesslog.demo.uitls.UUIDUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by xuliugen on 2016/7/26.
 */
@Controller
@RequestMapping("/user")
public class UserController {

    @Inject
    private UserInfoApplication userInfoApplication;

    @ResponseBody
    @RequestMapping("/add")
    Response addUser(HttpServletRequest request) {

        UserInfo userInfo = new UserInfo();
        String str = UUIDUtils.get32UUIDAsLowerCase();
        userInfo.setIdUserInfo(str);
        userInfo.setUserAccount(str);
        userInfo.setUserPwd(str);
        userInfo.setIsValid(str);
        userInfo.setUserTypeCode(str);
        userInfo.setUserAvatar(str);

        Response response = new Response();
        boolean flag = userInfoApplication.addUser(userInfo);
        if (flag) {
            response.setMsg("success");
            request.getSession().setAttribute("USER", userInfo);
        } else {
            response.setMsg("failed");
        }

        return response;
    }
}
