package com.ufind.businesslog.demo.domain.response;

import java.io.Serializable;

public interface IResponse extends Serializable {

    /**
     * 是否是成功的响应
     * @return
     */
    boolean isSuccess();
}
