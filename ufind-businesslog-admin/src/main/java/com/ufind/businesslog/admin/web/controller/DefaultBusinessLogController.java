package com.ufind.businesslog.admin.web.controller;

import com.ufind.businesslog.admin.application.BusinessLogApplication;
import com.ufind.businesslog.admin.common.page.Page;
import com.ufind.businesslog.admin.dto.DefaultBusinessLogDTO;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/log")
public class DefaultBusinessLogController {

    @Inject
    private BusinessLogApplication businessLogApplication;

    @ResponseBody
    @RequestMapping("/list")
    public Page<DefaultBusinessLogDTO> pageJson(DefaultBusinessLogDTO defaultBusinessLogDTO,
                                                @RequestParam int page,
                                                @RequestParam int pagesize) {
        return businessLogApplication.pageQueryDefaultBusinessLog(defaultBusinessLogDTO, page, pagesize);
    }

    @ResponseBody
    @RequestMapping("/delete")
    public Map<String, Object> delete(@RequestParam String ids) {
        Map<String, Object> result = new HashMap<String, Object>();
        String[] idStrs = ids.split(",");
        businessLogApplication.removeDefaultBusinessLogs(idStrs);
        result.put("result", "success");
        return result;
    }

    @ResponseBody
    @RequestMapping("/get/{id}")
    public Map<String, Object> get(@PathVariable String id) {
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("data", businessLogApplication.getDefaultBusinessLog(id));
        return result;
    }

}