package com.ufind.businesslog.demo.exportImpl;

import com.ufind.businesslog.api.BusinessLog;
import com.ufind.businesslog.api.BusinessLogExporter;
import com.ufind.businesslog.demo.application.BusinessLogApplication;
import com.ufind.businesslog.demo.domain.DefaultBusinessLog;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * 日志导出器默认实现
 * User: zjzhai
 * Date: 3/5/14
 * Time: 10:05 AM
 */
@Named
public class BusinessLogExporterImpl implements BusinessLogExporter {

    @Inject
    private BusinessLogApplication businessLogApplication;

    public void export(BusinessLog businessLog) {
        businessLogApplication.save(DefaultBusinessLog.createBy(businessLog));
    }
}
