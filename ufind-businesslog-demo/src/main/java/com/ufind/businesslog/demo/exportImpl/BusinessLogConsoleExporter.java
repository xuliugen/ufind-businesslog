package com.ufind.businesslog.demo.exportImpl;

import com.ufind.businesslog.api.BusinessLog;
import com.ufind.businesslog.api.BusinessLogExporter;

/**
 * 日志导出器默认实现
 * User: zjzhai
 * Date: 3/5/14
 * Time: 10:05 AM
 */
public class BusinessLogConsoleExporter implements BusinessLogExporter {

    public void export(BusinessLog businessLog) {
        System.out.println(businessLog);
    }

}
