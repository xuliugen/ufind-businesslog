package com.ufind.businesslog.admin.domain;

import com.google.common.collect.Lists;
import com.ufind.businesslog.admin.common.page.Page;
import com.ufind.businesslog.admin.dto.DefaultBusinessLogDTO;
import com.ufind.businesslog.admin.infra.repository.sql.DefaultBusinessLogMapper;
import com.ufind.businesslog.admin.utils.UUIDUtils;
import com.ufind.businesslog.api.BusinessLog;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.ufind.businesslog.api.constant.ContextKeyConstant.BUSINESS_OPERATION_IP;
import static com.ufind.businesslog.api.constant.ContextKeyConstant.BUSINESS_OPERATION_USER;

/**
 * User: zjzhai Date: 12/12/13 Time: 11:23 AM
 */
@Named
public class DefaultBusinessLog extends DefaultBusinessLogDTO {

    @Inject
    private DefaultBusinessLogMapper defaultBusinessLogMapper;

    public synchronized static DefaultBusinessLog createBy(BusinessLog businessLog) {
        DefaultBusinessLog myBusinessLog = new DefaultBusinessLog();
        Map<String, Object> context = businessLog.getContext();

        if (context.get(BUSINESS_OPERATION_USER) != null) { //设置操作人
            myBusinessLog.setUser((String) context.get(BUSINESS_OPERATION_USER));
        }

//        if (context.get(BUSINESS_OPERATION_TIME) != null) { //设置操作时间
//            myBusinessLog.setTime((Date) context.get(BUSINESS_OPERATION_TIME));
//        }

        if (context.get(BUSINESS_OPERATION_IP) != null) { //设置操作IP
            myBusinessLog.setIp((String) context.get(BUSINESS_OPERATION_IP));
        }

        myBusinessLog.setLog(businessLog.getLog()); //设置操作日志内容
        myBusinessLog.setCategory(businessLog.getCategory()); //设置操作类别

        context = Collections.unmodifiableMap(context);

        return myBusinessLog;
    }

    /**
     * 日志的保存操作
     */
    public boolean save(DefaultBusinessLog log) {
        log.setId(UUIDUtils.get32UUIDAsLowerCase());
        return defaultBusinessLogMapper.insertSelective(log) == 1 ? true : false;
    }

    public DefaultBusinessLog getById(String id) {
        return defaultBusinessLogMapper.selectByPrimaryKey(id);
    }

    public boolean updateLog(DefaultBusinessLog defaultBusinessLog) {
        return defaultBusinessLogMapper.updateByPrimaryKeySelective(defaultBusinessLog) == 1 ? true : false;
    }

    public boolean removeBusinessLog(String id) {
        return defaultBusinessLogMapper.deleteByPrimaryKey(id) == 1 ? true : false;
    }

    public Page<DefaultBusinessLog> pageQueryDefaultBusinessLog(int currentPage, int pageSize, DefaultBusinessLogDTO queryVo) {

        Page<DefaultBusinessLog> logPage = new Page<DefaultBusinessLog>();
        if (queryVo == null) {
            queryVo = new DefaultBusinessLogDTO();
        }
        int resultCount = defaultBusinessLogMapper.countDefaultBusinessLog(queryVo);
        logPage.setPage(pageSize, 0, currentPage);
        if (resultCount > 0) {
            List<DefaultBusinessLog> list = defaultBusinessLogMapper.pageQueryDefaultBusinessLog(currentPage * pageSize, pageSize, queryVo);
            logPage.setData(list);
            logPage.setResultCount(resultCount);
        } else {
            logPage.setData(Lists.newArrayList(new DefaultBusinessLog()));
            logPage.setResultCount(0);
        }
        return logPage;
    }
}
