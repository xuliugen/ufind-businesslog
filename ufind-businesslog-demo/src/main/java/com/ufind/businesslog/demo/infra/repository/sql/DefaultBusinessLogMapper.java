package com.ufind.businesslog.demo.infra.repository.sql;

import com.ufind.businesslog.demo.domain.DefaultBusinessLog;
import org.springframework.stereotype.Component;

/**
 * Created by xuliugen on 2016/7/26.
 */
@Component
public interface DefaultBusinessLogMapper {

    Long insertSelective(DefaultBusinessLog log);
}
