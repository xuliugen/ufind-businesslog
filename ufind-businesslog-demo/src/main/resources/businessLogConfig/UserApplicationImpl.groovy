package businessLogConfig

class UserInfoApplicationImpl {

    def context

    def UserInfoApplicationImpl_addUser() {
        "${getPreTemplate()}:创建一个新用户:${context._param0.userAccount}"
    }

    def getPreTemplate() {
        "${context._user}-"
    }

}