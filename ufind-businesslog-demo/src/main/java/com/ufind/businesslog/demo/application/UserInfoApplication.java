package com.ufind.businesslog.demo.application;

import com.ufind.businesslog.demo.domain.UserInfo;

/**
 * Created by xuliugen on 2016/7/26.
 */
public interface UserInfoApplication {

    public boolean addUser(UserInfo userInfo);
}
