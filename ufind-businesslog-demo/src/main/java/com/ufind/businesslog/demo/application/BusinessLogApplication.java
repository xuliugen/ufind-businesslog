package com.ufind.businesslog.demo.application;

import com.ufind.businesslog.demo.domain.DefaultBusinessLog;

/**
 * User: zjzhai
 * Date: 12/11/13
 * Time: 3:11 PM
 */
public interface BusinessLogApplication {

    boolean save(DefaultBusinessLog log);

}
