package com.ufind.businesslog.demo.application.impl;

import com.ufind.businesslog.api.BusinessLogAlias;
import com.ufind.businesslog.demo.application.UserInfoApplication;
import com.ufind.businesslog.demo.domain.UserInfo;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by xuliugen on 2016/7/26.
 */
@Named
public class UserInfoApplicationImpl implements UserInfoApplication {

    @Inject
    private UserInfo userInfo;

    @BusinessLogAlias("UserInfoApplicationImpl_addUser")
    public boolean addUser(UserInfo userInfo) {
        return this.userInfo.addUser(userInfo);
    }

}
